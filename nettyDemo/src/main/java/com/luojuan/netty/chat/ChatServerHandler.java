package com.luojuan.netty.chat;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.DefaultEventExecutor;

public class ChatServerHandler extends SimpleChannelInboundHandler {

    private static ChannelGroup channelGroup = new DefaultChannelGroup(new DefaultEventExecutor());

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        Channel channel = ctx.channel();
        channelGroup.add(channel);
        super.channelActive(ctx);
        channelGroup.writeAndFlush(channel.remoteAddress()+"上线啦");
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object msg) throws Exception {
       Channel channel = channelHandlerContext.channel();
       channelGroup.forEach(ch->{
           if(ch.equals(channel)){
               ch.writeAndFlush("【我】说:"+msg);
           }else{
               ch.writeAndFlush(ch.remoteAddress()+"说:"+msg);
           }
       });
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        Channel channel = ctx.channel();
        channelGroup.remove(channel);
        channel.writeAndFlush(channel.remoteAddress()+"下线了");
    }
}
