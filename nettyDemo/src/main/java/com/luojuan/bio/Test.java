package com.luojuan.bio;

import java.lang.reflect.InvocationTargetException;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class Test {

    public static void main(String[] args) {
        new Thread(()->{
            try{
                Server.start();
            }catch (Exception e){
                e.printStackTrace();
            }
        }).start();
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        char[] op = {'+','-','*','/'};
        Random random  =  new Random(System.currentTimeMillis());
        new Thread(()->{
            while (true){
                String expression = random.nextInt(10) + "" + op[random.nextInt(4)] + (random.nextInt(10)+1);
                Client.send(expression);
                try {
                    TimeUnit.SECONDS.sleep(3);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        }).start();
    }


}
